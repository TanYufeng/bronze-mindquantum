![](qworld/images/readme-logo.jpg)

# [QWorld](https://qworld.net)'s Bronze-MindQuantum

**Bronze** is our introductory tutorial on _**quantum computing and quantum programming**_ created in October 2018.

**Bronze-MindQuantum** is the version of Bronze using [**MindQuantum**](https://www.mindspore.cn/mindquantum/docs/en/master/index.html) as the quantum programming framework. Its QWorld version was released in August 2022. It was adapted from [Bronze-ProjectQ](https://gitlab.com/qworld/bronze-projectq) in two rounds.

Bronze is a collection of Jupyter notebooks, and each notebook has many programming tasks to provide hands-on experiences. We see Bronze as a laboratory where you can learn the basics of quantum computing and quantum programming by doing. Bronze has already been used in more than 90 workshops ([the most recent list](http://qworld.net/workshop-bronze/#list)) under QWorld by the end of July 2022. As a pedagogical strategy, we skip to use complex numbers to keep the tutorial simpler. 

## Prerequisite

The only prerequisite is to know the basics of programming (variables and basic data types, loops, and conditionals). Any previous experience in python will be helpful. If you do not have any such experince, you can check our notebooks on python before starting the tutorial.

Bronze has notebooks on the basic math to review your knowledge on the basic operations on vectors and matrices.

## Bronze-MindQuantum's sections

- Python (for a quick review)
- Basic math (for a quick review)
- Classical systems: bits, coin-flipping, probabilistic state and operators, composite systems, correlation, and controlled operators
- Quantum systems with ProjectQ
    - MindQuantum basics: circuit design, visualization, and simulation
    - quantum basics: quantum coin-flipping and Hadamard operator, quantum states and opeators, visualization of a real-valued qubit, superposition and measurements
    - quantum operators on a real-valued single qubit (rotations and reflections) and quantum tomography
    - entanglement and basic quantum protocols superdense coding and quantum teleportation
    - Grover's search algorithm

Our following elementrary level tutorial _Silver_ can be accessed at [here](https://gitlab.com/qworld/silver).

## Contribution

Please make a merge request or create an issue for _reporting typo_ or _your corrections_.

Please create an issue for _your questions_, _initiating a discussion_, or _proposing a contribution_.

_Bronze has been developed under [QEducation departmant](https://qworld.net/qeducation/) of QWorld._

## Installation

1. Install [Anaconda](https://www.anaconda.com/products/distribution)

1. Get a copy of [Bronze-MindQuantum](https://gitlab.com/qworld/bronze-mindquantum)

	1. You may clone it on your local computer, or,
	1. you may download the source code and then upzip it on your local computer.
	1. _If you are a beginner, keep your local copy on your desktop for easy access._

1. Anaconda -> Jupyter Notebook -> Access the local copy of our repo -> Open the file "START.ipynb"

1. Go to the notebook "MindQuantum Installation and Test" and complete your instalation

## License

The text and figures are licensed under the Creative Commons Attribution 4.0 International Public License (CC-BY-4.0), available at https://creativecommons.org/licenses/by/4.0/legalcode. 

The code snippets in the notebooks are licensed under Apache License 2.0, available at http://www.apache.org/licenses/LICENSE-2.0.

## Acknowledgements

We use [MathJax](https://www.mathjax.org) to display mathematical expressions on html files (e.g., exercises).

We use open source interactive tool [quantumgame](http://play.quantumgame.io) for showing quantum coin flipping experiments.

## Credits

Bronze was created by [Abuzer Yakaryilmaz](http://abu.lu.lv) (QWorld & QLatvia) in October 2018, and it has been developed and maintained by him. 

Özlem Salehi Köken (QWorld & QTurkey) and Maksims Dimitrijevs (QWorld & QLatvia) are the other contributors by preparing new notebooks and revising the existing notebooks.

Bronze was publicly available on July 7, 2019.

[The first round of MindQuantum adaptation](https://gitee.com/Huawei-HiQ/bronze-mindquantum) was done by Junyuan Zhou and Xu Zhou based on [Bronze-ProjectQ](https://gitlab.com/qworld/bronze-projectq) by the end of 2021. 
[The second round of MindQuantum adaptation](https://gitlab.com/qworld/bronze-mindquantum) was done by Abuzer Yakaryilmaz in July 2022. 
A revision was made by Marija Šćekić in August 2022.


### Video lectures

The recording lectures for classical systems were prepared by Abuzer Yakaryilmaz in August 2020.

### Bronze 2020 & 2021 & 2022

We thank to the participants of QBronze workshops and [QTraining for Bronze program](https://qworld.net/qtraining-for-bronze-2020/) for their corrections and suggestions.

### Bronze 2019

We thank to the mentors and participants of [QDrive](https://qworld.net/qdrive/) for their very helpful corrections and suggestions.

We thank Adam Glos (QWorld & QPoland) for his comments on Bronze 2018.

### Bronze 2018

We thank to Katrina Kizenbaha from Riga TechGirls for her revisions on our notebooks on python.

We thank to Martins Kalis (QLatvia) for his technical comments on python, qiskit, and our notebooks.

We thank to Maksims Dimitrijevs (QLatvia) for his careful reading and corrections on our notebooks.

We thank to QLatvia's first members Martins Kalis, Maksims Dimitrijevs, Aleksejs Naumovs, Andis Draguns, and Matiss Apinis for their help and support.

We thank to the students of [Faculty of Computing](https://www.df.lu.lv) (University of Latvia) attending quantum programming's meetings on each Friday (Fall 2018) for their comments while working with our notebooks.